﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BeatData
{
    [SerializeField] private List<ChoiceData> _choices; //each point in the game has choices.
    [SerializeField] private string _text; //each beat/ point in the game has its text and its id. 
    [SerializeField] private int _id;

    public List<ChoiceData> Decision { get { return _choices; } }
    public string DisplayText { get { return _text; } }

    public void SetText(string temp) 
    {
        _text = temp;
    }
    public int ID { get { return _id; } }
}
