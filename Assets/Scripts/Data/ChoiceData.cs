﻿using System;
using UnityEngine;

[Serializable]
public class ChoiceData
{
    [SerializeField] private string _text; //each choice has its text and a beat id (which point that choice leads to)
    [SerializeField] private int _beatId;

    private bool chosen = false;

    public string DisplayText 
    { 
        get { return _text; } 
    
    }

    public bool GetChosen
    {
        get { return chosen; }
    }
    public void SetChosen(bool temp)
    {
        chosen = temp;
    }
    public int NextID { get { return _beatId; } }
}
