﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveLevel : MonoBehaviour
{
    private int nextSceneIndex = 0;
    private GameObject passBool;

    [SerializeField]
    private GameObject passUI;

    private float timer;
    private bool Pass = false;

    static public int winScore;

    // Start is called before the first frame update
    void Start()
    {
        passBool = GameObject.Find("Player");
        
        timer = 0;
        nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        

    }

    // Update is called once per frame
    void Update()
    {
       
        Pass = passBool.GetComponent<PlayerMoves>().pass; // has the player passed the level

        if (Pass == true)
        {
            if (winScore < 4) // as long as the player hasn't finished the the game
            {
                passUI.SetActive(true);

                timer += 1 * Time.deltaTime;
                if (timer >= 15 && nextSceneIndex < SceneManager.sceneCountInBuildSettings) 
                {
                    winScore += 1;
                    SceneManager.LoadScene(nextSceneIndex);
                    timer = 0;
                    passUI.SetActive(false);
                    Pass = false;


                }
            }
            
            
        }
        


        if (winScore >= 4) // if player wins load win scene
        {
            SceneManager.LoadScene(6);
            
        }

        
    }

    
}
