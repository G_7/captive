﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class CluesUI : MonoBehaviour
{
    private int counter, clueNum;

    private List<Clues> cl;
    private GameObject clueList;

    [SerializeField]
    private TMP_Text clue;

    [SerializeField]
    private GameObject clueNoteCard;

    private bool isClicked;

    private GameObject sounds;
    private Audio a;

    [SerializeField]
    private List<string> cluesText;
    // Start is called before the first frame update
    void Start()
    {
        switch (MoveLevel.winScore) //setting the counter for each level
        {
            case 0:
                counter = 0;
                break;
            case 1:
                counter = 3;
                break;
            case 2:
                counter = 6;
                break;
            case 3:
                counter = 9;
                break;

        }
        clueNum = 0;
        cl = new List<Clues>();
        clueList = GameObject.Find("CluesControl");
        clueNoteCard.SetActive(false);
        sounds = GameObject.Find("SM");
        a = sounds.GetComponent<Audio>();
    }

    // Update is called once per frame
    void Update()
    {
        cl = clueList.GetComponent<SearchingInteractions>().GetClues();
        if (clueNum > 2)
        {
            clueNum = 0;
        }
        switch (MoveLevel.winScore) // so the correct clues show in the correct level
        {
            case 0:
                ChangeText(0, 2);
                break;
            case 1:
                ChangeText(3, 5);
                break;
            case 2:
                ChangeText(6, 8);
                break;

            case 3:
                ChangeText(9, 11);
                break;

        }
    }

    public void ReadClues() //the next clue button
    {
        a.PlaySound("Click");
        if (cl[clueNum].GetFound == true)
        {

            clue.text = cluesText[counter];
        }
        else
        {
            clue.text = " ";
        }
        counter++;
        clueNum++;

        

        
    }

    void ChangeText(int begin, int end) // change between clues 1, 2 and 3
    {
        
        if (counter > end) 
        {
            counter = begin;
        }
    }
    public void Clicked()
    {
        a.PlaySound("Click");
        ViewClues();
    }
    private void ViewClues() // Clues button, disable and enable the clues UI
    {
        isClicked = !isClicked;
        if (isClicked)
        {
            clueNoteCard.SetActive(true);
            Time.timeScale = 0f;
        }
        else
        {
            clueNoteCard.SetActive(false);
            Time.timeScale = 1f;
        }
    }

    public void ExitGame() 
    {
        a.PlaySound("Click");
        Application.Quit();
    }
}
