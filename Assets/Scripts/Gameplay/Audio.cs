﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Audio : MonoBehaviour
{
    public List<AudioClip> audio1; // list of sounds
    
    private AudioSource aSource; 


    
    // Start is called before the first frame update
    void Start()
    {
        aSource = GetComponent<AudioSource>();
        
    }

   
    
   public void PlaySound(string sound) //loop through the list if the name used when the function is called equals a name in the list play that sound
   {
        for (int i = 0; i < audio1.Count; i++) 
        {
            if (sound == audio1[i].name)
            {
                aSource.clip = audio1[i];
                
            }

        }
        
        aSource.Play();
        
   }

}
