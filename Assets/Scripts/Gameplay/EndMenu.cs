﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class EndMenu : MonoBehaviour
{
    //[SerializeField]
    //private GameObject start, end;

    //[SerializeField]
    //private TMP_Text outcomeText;

    //private string win, lose;


    GameObject sounds;
    Audio a;

    // Start is called before the first frame update
    void Start()
    {
        //win = "You win! That means you're free, right?";

        //lose = "Oh no, looks like you lost.";
        sounds = GameObject.Find("SM");
        a = sounds.GetComponent<Audio>();
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }

    public void Play()
    {
        MoveLevel.winScore = 0;
        SceneManager.LoadScene(1);
        a.PlaySound("Click");
    }
    public void ExitGame()
    {
        a.PlaySound("Click");
        Application.Quit();
    }
}
