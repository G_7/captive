﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System;

public class SearchingInteractions : MonoBehaviour
{
    //[SerializeField]
    private List<Clues> clues;

    [SerializeField]
    private List<string> clueText;

    
    // Start is called before the first frame update
    void Start()
    {
        clues = new List<Clues>();
        ClueSetup();
    }

    
   
    void ClueSetup()
    {
        System.Random id = new System.Random(); 

        for (int i = 0; i < 3; i++)
        {
            int ID = id.Next(1, 4);
            Clues clue = new Clues(); // create clues
            //clue.SetID(ID);

            clues.Add(clue); // add them the clue list
            clues[i].SetID(ID); // give random location id
            clues[i].SetText(clueText[i]);
        }
    }
    public List<Clues> GetClues() // get clue list
    {
        return clues;
    }
    public void SetClues(List<Clues> l) // set clue list
    {
        clues = l;
    }



}
