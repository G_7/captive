﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerMoves : MonoBehaviour
{
    [SerializeField]
    private List<string> animNames;


    private BeatData curreentBeat;
    

    [SerializeField]
    private StoryData beat;

    private GameObject currentB;

    [SerializeField]
    private List<TMP_Text> clueText;

    private int count = 0, id = 0, died;

    [SerializeField]
    private List<Transform> searchPos;

    private NavMeshAgent na;

    private float searchTimer;

    private bool searched;

    private string searchPlace;

    private GameObject clueList;

    private List<Clues> cl;

    public bool pass;

    private GameObject sounds;

    private Audio a;

    

    // Start is called before the first frame update
    void Start()
    {
        currentB = GameObject.Find("StoryControl");
        clueList = GameObject.Find("CluesControl");
        
        searchTimer = 0;
        na = this.GetComponent<NavMeshAgent>();
        cl = new List<Clues>();
        
       
        searched = false;
        id = 0;
        searchPlace = " ";
        pass = false;
        died = 0;

        sounds = GameObject.Find("SM");
        a = sounds.GetComponent<Audio>();
    }

    // Update is called once per frame
    void Update()
    {

        if (searched == true) // Time it takes to search
        {
            searchTimer += 1 * Time.deltaTime;
        }
        if (searchTimer >= 3f)
        {
            searchTimer = 0;
            searched = false;


        }

        curreentBeat = currentB.GetComponent<Game>()._currentBeat; //the current beat
        cl = clueList.GetComponent<SearchingInteractions>().GetClues(); //List of clues

        if (curreentBeat != null) 
        {
            if (curreentBeat.ID == 3) //When the searching happens
            {
                if (count >= 1)
                {
                    curreentBeat.SetText("You are searching...");
                }
                else
                {
                    curreentBeat = beat.GetBeatById(3);
                    curreentBeat.SetText(curreentBeat.DisplayText);
                }
                for (int i = 0; i < cl.Count; i++) // access the list of choices for beat 3
                {
                    ChoiceData choices = curreentBeat.Decision[i];

                    if (choices.GetChosen == true) // if the choice has been choosen move player to postion. 
                    {
                        MovePlayer(searchPos[i].position);
                        count++;
                    }
                    choices.SetChosen(false);
                }


            }

            if (curreentBeat.ID == 7)
            {
                MovePlayer(searchPos[searchPos.Count - 1].position);
                pass = true;
            }
            if (curreentBeat.ID == 4 || curreentBeat.ID == 6) // if player chooses wrong 
            {
                currentB.GetComponent<Lightd>().Trouble = true; 
                died = 1; // die
            }
            else
            {
                currentB.GetComponent<Lightd>().Trouble = false;
            }
        }
        
        FindClue();
        

        for (int i = 0; i < cl.Count; i++)
        {
            if (cl[i].GetID == id && searched == false) // if searching has finished and id of clue is equal to local id ...
            {
                cl[i].SetFound(true);
                clueText[i].text = cl[i].GetText + " found"; // Setting the clue to found if it is found
                
                
            }
            
        }

        if (died == 1) 
        {
            SceneManager.LoadScene(5); // if player has died load end of game menu
        }
        clueList.GetComponent<SearchingInteractions>().SetClues(cl);
        
       
    }

    private void MovePlayer(Vector3 target) // function to move player to whatever postion
    {
        if (na != null) 
        {
            
            na.SetDestination(target);
        }
        
    }


    void PlayAnim(Collider other, string names, bool yes, string trigger, int b) //plays the animations when player searches
    {
        if (other.name == names)
        {
            //anim.Play("S_Drawer");
            //other.gameObject.transform.position += new Vector3(0, 0, 10f * Time.deltaTime);
            other.gameObject.GetComponent<Animator>().SetTrigger(trigger);
            searched = yes;
            //id += a;
            searchPlace = names;
            if (yes == true) 
            {
                a.PlaySound("Search");
            }
            
        }
    }
    private void OnTriggerEnter(Collider other) // play animation
    {
        for (int i = 0; i< animNames.Count; i++) 
        {
            PlayAnim(other, animNames[i], true, "Open", 1);
            
            
        }

        

    }
    private void OnTriggerExit(Collider other)
    {
        for (int i = 0; i < animNames.Count; i++)
        {
            PlayAnim(other, animNames[i], false, "Close", 0);
        }
        
       
    }

    void FindClue() 
    {
        for (int i = 0; i < 3; i++) // each clue has id for the place it is found
        {
            if (searchPlace == animNames[i])
            {
                id = (i + 1); // setting a local id variable the 1, 2 or 3 depending on location
            }
        }
        
    
    }
}
