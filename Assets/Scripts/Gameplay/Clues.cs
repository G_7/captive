﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clues //clues class
{
   [SerializeField] private Vector3 position;
   [SerializeField] private string text;
   [SerializeField] private bool found = false;
   [SerializeField] private int locationID;

    public void SetPos(Vector3 temp) 
    {
        position = temp;
    }

    public Vector3 GetPos 
    {
        get { return position; }
    }

    public void SetID(int temp) //clues id
    {
        locationID = temp;
    }

    public int GetID
    {
        get { return locationID; }
    }

    public void SetText(string temp)
    {
        text = temp;
    } //clue text

    public string GetText
    {
        get { return text; }
    }

    public void SetFound(bool temp)
    {
        found = temp;
    } //if the clue is found

    public bool GetFound
    {
        get { return found; }
    }


}
