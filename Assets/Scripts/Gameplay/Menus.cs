﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menus : MonoBehaviour
{
    [SerializeField]
    private GameObject start, info;

    private GameObject sounds;
    private Audio a;
    
    // Start is called before the first frame update
    void Start()
    {
        sounds = GameObject.Find("SM");
        a = sounds.GetComponent<Audio>();
    }

    // Update is called once per frame
    
    public void Play() //play
    {
        a.PlaySound("Click");
        MoveLevel.winScore = 0;
        SceneManager.LoadScene(1);
    }
    public void ExitGame() //exit
    {
        a.PlaySound("Click");
        Application.Quit();
    }

    public void Info() //info
    {
        a.PlaySound("Click");
        start.SetActive(false);
        info.SetActive(true);
    }
    public void Back() //back to start menu
    {
        a.PlaySound("Click");
        start.SetActive(true);
        info.SetActive(false);
    }
    

}
